# ToDo_list_with_user_authentication



## Getting started

To Do list app with User Registration, Login, Search and full Create Read Update and DELETE functionality.

***
## About This App
1. User Can Register 
2. User Can Add Todo, edit that todo and also delete that todo


![](images/todo1.png)
![](images/todo2.png)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:62d810640f5d5b7c7d1376e8948b2b7f?https://docs.gitlab.com/ee/ci/quick_start/index.html)
